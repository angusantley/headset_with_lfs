﻿using UnityEngine;
using System.Collections;
using RealisticEyeMovements;

public class handle_lookats_cg : MonoBehaviour
{

    private LookTargetController looker;
    public Transform person;
    Transform person2;
    Transform person3;
    Transform participant;
    Transform target_of_attention;
    private AudioSource audioplayer;
    bool notlooked;
    bool notlooked2;
    float timedelay;
    float duration;

    // Use this for initialization
    void Start()
    {

        looker = transform.GetComponent<LookTargetController>();
        //person = GameObject.Find("heidi").transform.Find("RL_G6Beta_Hips").Find("RL_G6Beta_Spine01").Find("RL_G6Beta_Spine02").Find("RL_G6Beta_Spine03").Find("RL_G6Beta_Neck").Find("RL_G6Beta_Head").transform;
        person2 = GameObject.Find("man1").transform.Find("RL_G6Beta_Hips").Find("RL_G6Beta_Spine01").Find("RL_G6Beta_Spine02").Find("RL_G6Beta_Spine03").Find("RL_G6Beta_Neck").Find("RL_G6Beta_Head").transform;
        person3 = GameObject.Find("man2").transform.Find("RL_G6Beta_Hips").Find("RL_G6Beta_Spine01").Find("RL_G6Beta_Spine02").Find("RL_G6Beta_Spine03").Find("RL_G6Beta_Neck").Find("RL_G6Beta_Head").transform;
        participant = GameObject.Find("Main Camera").transform;
        audioplayer = transform.GetComponent<AudioSource>();
        notlooked = true;
        notlooked2 = true;
        timedelay = -1.0f;
        duration = 0.0f;
        target_of_attention = person;

    }

    // Update is called once per frame
    void Update()
    {
        if (audioplayer.isPlaying)//  && notlooked)
        {

            if ((audioplayer.clip.name == "anxiety1_line1_teacher_cg") && notlooked)
            {
                Debug.Log("line 1 lookat");
                Debug.Log(audioplayer.clip.samples);
                Debug.Log(audioplayer.timeSamples);
                //looker.LookAtPoiDirectly(person, 1.0f, 0.1f);
                target_of_attention = person2;
                duration = 3.0f;
                timedelay = 0.01f;
                notlooked = false;
                looker.LookAtPoiDirectly(target_of_attention, duration, 0.01f);
            }
            if ((audioplayer.clip.name == "anxiety1_line1_teacher_cg") && (audioplayer.time > 18.0f) && notlooked2)
            {
                Debug.Log("line 1 2nd lookat");
                Debug.Log(audioplayer.time);
                //looker.LookAtPoiDirectly(person, 1.0f, 0.1f);
                target_of_attention = person2;
                duration = 13.0f;
                timedelay = 0.01f;
                notlooked2 = false;
                looker.LookAtPoiDirectly(target_of_attention, duration, 0.01f);


            }
            if ((audioplayer.clip.name == "anxiety1_line3_teacher_cg") && notlooked)
            {
                Debug.Log("line 3 lookat");
                //looker.LookAtPoiDirectly(person2, 1.0f, 0.1f);
                target_of_attention = person2;
                duration = 3.0f;
                timedelay = 0.01f;
                notlooked = false;
                looker.LookAtPoiDirectly(target_of_attention, duration, 0.01f);
            }

            if ((audioplayer.clip.name == "anxiety1_line3_teacher_cg") && (audioplayer.time > 3.5f) && notlooked2)
            {
                Debug.Log("line 3 lookat 2");
                //looker.LookAtPoiDirectly(person2, 1.0f, 0.1f);
                target_of_attention = person;
                duration = 8.0f;
                timedelay = 0.01f;
                notlooked2 = false;
                looker.LookAtPoiDirectly(target_of_attention, duration, 0.01f);
            }

            if ((audioplayer.clip.name == "anxiety1_line5_teacher_cg") && notlooked)
            {
                Debug.Log("line 5 lookat");
                //looker.LookAtPoiDirectly(person2, 1.0f, 0.1f);
                target_of_attention = person;
                duration = 0.3f;
                timedelay = 0.01f;
                notlooked = false;
                looker.LookAtPoiDirectly(target_of_attention, duration, 0.01f);
            }
            if ((audioplayer.clip.name == "anxiety1_line5_teacher_cg") && (audioplayer.time > 0.5f) && notlooked2)
            {
                Debug.Log("line 5 lookat2");
                //looker.LookAtPoiDirectly(person2, 1.0f, 0.1f);
                target_of_attention = participant;
                duration = 10.0f;
                timedelay = 0.01f;
                notlooked2 = false;
                looker.LookAtPoiDirectly(target_of_attention, duration, 0.01f);
            }

            if ((audioplayer.clip.name == "anxiety1_line6_teacher_cg") && notlooked)
            {
                Debug.Log("line 6 lookat");
                //looker.LookAtPoiDirectly(person2, 1.0f, 0.1f);
                target_of_attention = participant;
                duration = 4.0f;
                timedelay = 0.01f;
                notlooked = false;
                //looker.LookAtPoiDirectly(target_of_attention, duration, 0.01f);
            }
            if ((audioplayer.clip.name == "anxiety1_line7_teacher_cg") && notlooked)
            {
                Debug.Log("line 7 lookat");
                //looker.LookAtPoiDirectly(person2, 1.0f, 0.1f);
                target_of_attention = participant;
                duration = 4.0f;
                timedelay = 0.01f;
                notlooked = false;
                //looker.LookAtPoiDirectly(target_of_attention, duration, 0.01f);
            }
            if ((audioplayer.clip.name == "anxiety1_line8_teacher_cg") && (audioplayer.time > 4.5f) && notlooked)
            {
                Debug.Log("line 8 lookat");
                //looker.LookAtPoiDirectly(person2, 1.0f, 0.1f);
                target_of_attention = person3;
                duration = 8.0f;
                timedelay = 0.01f;
                notlooked = false;
                looker.LookAtPoiDirectly(target_of_attention, duration, 0.01f);
            }
            if ((audioplayer.clip.name == "anxiety1_line9_teacher_cg") && (audioplayer.time > 3.2f) && notlooked)
            {
                Debug.Log("line 9 lookat");
                //looker.LookAtPoiDirectly(person2, 1.0f, 0.1f);
                target_of_attention = person3;
                duration = 8.0f;
                timedelay = 0.01f;
                notlooked = false;
            }

        }
        if (!(audioplayer.isPlaying))
        {
            notlooked = true;
            notlooked2 = true;
            //timedelay = 0.0f;
        }

        if (Input.GetKeyDown("k"))
        {
            //looker.LookAtPoiDirectly(person, 1.0f, 0.1f);
            Debug.Log("clip name " + audioplayer.clip.name);
        }
        /*
        if ( !notlooked && ( timedelay > 0.0f ) )
        {
            timedelay -= Time.deltaTime;
            if ( timedelay < 0.0f )
            {
                Debug.Log("look at now");
                looker.LookAtPoiDirectly(target_of_attention, duration, 0.01f);
            }
        }
         */

    }
}
