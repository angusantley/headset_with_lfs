﻿using UnityEngine;
using System.Collections;
using RealisticEyeMovements;

public class grouplookingbehaviour : MonoBehaviour {

    public Transform[] people;

    public void allatparticipant()
    {
        for (int i = 0; i < people.Length; i++)
        {
            people[i].GetComponent<LookTargetController>().lookAtPlayerRatio = 1.0f;

        }


    }
    public void allawayfromparticipant()
    {
        for (int i = 0; i < people.Length; i++)
        {
            people[i].GetComponent<LookTargetController>().lookAtPlayerRatio = 0.01f;

        }


    }
    public void allturnoffREM()
    {
        for (int i = 0; i < people.Length; i++)
        {
            people[i].GetComponent<EyeAndHeadAnimator>().headWeight = 0.01f;

        }


    }
    public void allturnonREM()
    {
        for (int i = 0; i < people.Length; i++)
        {
            people[i].GetComponent<EyeAndHeadAnimator>().headWeight = 1.0f;
        }

    }

    public void headspeedup()
    {
        for (int i = 0; i < people.Length; i++)
        {
            people[i].GetComponent<EyeAndHeadAnimator>().headSpeedModifier = 2.7f;
        }

    }

	// Use this for initialization
	void Start () {

        headspeedup();
	
	}
	
	// Update is called once per frame
	void Update () {

        
	}
}
