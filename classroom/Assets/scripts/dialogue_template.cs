﻿using UnityEngine;
using System.Collections;

public class dialogue_template : MonoBehaviour {

    [System.Serializable]
    public class actor_line_time
    {
        public string Name;
        public Transform actor;
        public AudioClip actor_line;
        public float line_trigger_time;
        
        [HideInInspector]
        public bool notspoken; 
    }
    public actor_line_time[] screenplay;


	// Use this for initialization
	void Start () {

        for (int k = 0; k < screenplay.Length; k++)
        {
            screenplay[k].notspoken = true;
        }
        

        
	
	}
	
	// Update is called once per frame
	void Update () {

        if ( Input.GetKeyDown("j") )
        {
            //positive ending to scenario
            //fades.scene_end = true;
        }


        if (Input.GetKeyDown("k"))
        {
            //outcome: participant did not act
            //

        }


        for (int j = 0; j < screenplay.Length; j++)
        {
            //is it past time for this line? and has it yet to be spoken?
            if ( ( fades.scenetime > screenplay[j].line_trigger_time ) && ( screenplay[j].notspoken ) )
            {
                screenplay[j].notspoken = false;
                //trigger the line.
                AudioSource actorvoice = screenplay[j].actor.GetComponent<AudioSource>();
                changesound currentactorlines = screenplay[j].actor.GetComponent<changesound>();
                Animator actor_animator = screenplay[j].actor.GetComponent<Animator>();
                bool soundfound = false; 

                for (int i = 0; i < currentactorlines.clips.Length; i++)
                {
                    if (currentactorlines.clips[i].name == screenplay[j].actor_line.name)
                    {
                        soundfound = true;
                        actorvoice.clip = currentactorlines.clips[i];
                        actorvoice.Play();
                        if (actor_animator)
                        {
                            Debug.Log("set stand true in dialog 6");
                            actor_animator.SetBool("stand", true);
                            actor_animator.SetBool("sit", false);
                        }
                    }

                }
                if (!soundfound)
                {
                    //error this actor doesn't know this line
                    Debug.Log("line name not found in actor's line list!!");
                }

            }

        }
	
	}
}

