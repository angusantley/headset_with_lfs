﻿using UnityEngine;
using System.Collections;

public class usertalking_reactions : MonoBehaviour {

    [System.Serializable]
    public class user_talk
    {
        public char speakKey;
        public string motionchoice;
    }
    public user_talk[] responses_to_user;
    Animator anim;

	// Use this for initialization
	void Start () {
        anim = transform.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {

        for (int i = 0; i < responses_to_user.Length; i++)
        {

            if (Input.GetKeyDown(responses_to_user[i].speakKey.ToString()))
            {
              
                //start the reaction motion
                if (anim)
                {
                    anim.SetBool(responses_to_user[i].motionchoice.ToString(), true);
                }
            }
        }
	
	}
}
