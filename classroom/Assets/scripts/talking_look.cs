﻿using UnityEngine;
using System.Collections;
using RealisticEyeMovements;

public class talking_look : MonoBehaviour {

    private LookTargetController looker;
    private AudioSource audioplayer;

    public Transform[] lookats;
    public AudioClip[] lines;
    public float[] triggerpoints;
    public float[] durations;
   
    bool[] notlooked;
    //bool notlooked;
    //bool notlooked2;

    float duration;

	// Use this for initialization
	void Start () {

        looker = transform.GetComponent<LookTargetController>();
        audioplayer = transform.GetComponent<AudioSource>();
        //notlooked = true;
        //notlooked2 = true;
        notlooked = new bool[lines.Length] ;

        for (int k = 0; k < lines.Length; k++)
        {
            notlooked[k] = true;

        }

	}
	
	// Update is called once per frame
	void Update () {

        if (audioplayer.isPlaying)
        {
            //Debug.Log("line 1 lookat before audioclip.name");
            //Debug.Log(audioplayer.clip.name);
           // Debug.Log(lines[0].name);

            for (int i = 0; i < lines.Length; i++)
            {
                
                if ( audioplayer.clip.name == lines[i].name ) {
                    //Debug.Log("clipname is " + audioplayer.clip.name );
                    
                    if (audioplayer.time > triggerpoints[i]) {
                        //Debug.Log("past trigger point");
                        
                        if( notlooked[i]){
                            Debug.Log("not looked " + audioplayer.clip.name);
                            //Debug.Log("line " + i +  " talking look for " + transform.name.ToString());

                            notlooked[i] = false;
                            looker.LookAtPoiDirectly(lookats[i], durations[i], 0.01f);
                        }

                    }
                
                }
                
            }

        }else{

            for (int l = 0; l < lines.Length; l++)
            {
                notlooked[l] = true;

            }
           
        }

	
	}
}
