﻿using UnityEngine;
using System.Collections;

public class fades_updated : MonoBehaviour {


    ScreenFader fade;

    [System.Serializable]
    public class fade_info
    {
        //the place you are fading to.
        public Transform currentlocation;
        public Transform destination;
        //the scene time that you will begin the fade.
        public float beginfade;
        [HideInInspector]
        public bool fadestarted;
        [HideInInspector]
        public bool swaprooms;
        [HideInInspector]
        public bool fadeback;
    }

    public fade_info[] transitions;

    bool fadetoblack;
    bool swaprooms;
    bool fading;
    float fadetime;

    float movefactor;



    // Use this for initialization
    void Start()
    {

        fade = GameObject.Find("rootobject").GetComponent<ScreenFader>();
        movefactor = 1000.0f;

        for (int j = 0; j < transitions.Length; j++)
        {
            transitions[j].fadestarted = true;
            transitions[j].fadeback = true;
            transitions[j].swaprooms = true;
        }

    }

    // Update is called once per frame
    void Update()
    {

        if (dialogue_updated.scenetime > dialogue_updated.scene_end_time)
        {
            //fade to black at the end
            fade.fadeIn = false;
        }

        for (int i = 0; i < transitions.Length; i++)
        {
            //if time is greater than transition trigger time then fade to black
            if ((dialogue_updated.scenetime > transitions[i].beginfade) && transitions[i].fadestarted)
            {
                Debug.Log("starting a transition");
                transitions[i].fadestarted = false;
                fade.fadeIn = false;
            }

            //if time is greater than trans trigger + 3.0f then
            if ((dialogue_updated.scenetime > (transitions[i].beginfade + 3.0f)) && transitions[i].swaprooms)
            {
                transitions[i].swaprooms = false;
                //swap room positions when dark
                transitions[i].currentlocation.position = new Vector3(transitions[i].currentlocation.position.x, transitions[i].currentlocation.position.y + movefactor, transitions[i].currentlocation.position.z);
                //classplace.transform.position = new Vector3(classplace.position.x, classplace.position.y - movefactor, classplace.position.z);
                transitions[i].destination.position = new Vector3(transitions[i].destination.position.x, transitions[i].destination.position.y - movefactor, transitions[i].destination.position.z);

            }

            //if time greater that 6.0f past the trans trigger then fade back to light.
            if ((dialogue_updated.scenetime > (transitions[i].beginfade + 6.0f)) && transitions[i].fadeback)
            {
                transitions[i].fadeback = false;
                //end startroom
                //fade back in here 
                fade.fadeIn = true;
            }
        }


    }
}
