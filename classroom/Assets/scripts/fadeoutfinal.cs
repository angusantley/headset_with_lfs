﻿using UnityEngine;
using System.Collections;
using RealisticEyeMovements;

public class fadeoutfinal : MonoBehaviour {
    float movefactor;

    LookTargetController looker;
    LookTargetController looker1;
    LookTargetController looker2;
    LookTargetController looker3;
    LookTargetController looker4;
    LookTargetController looker5;
    LookTargetController looker6;

    ScreenFader fade;

    float fadetime;
    float endtime;
    float scenetime;
    float resumetime;
    float startroomtime;
    float endclasstime;
    float endintro;

    bool starting;
    bool fading;
    bool resuming;
    bool ending;
    bool scenario_running;
    bool lookatsubject;
    bool stoplookingatsubject;
    bool welldone;
    bool teacher_response;
    bool notswapped;

    // Use this for initialization
    void Start()
    {
        movefactor = 1000.0f;
        fadetime = 0.0f;
        endtime = 0.0f;
        scenetime = 0.0f;
        resumetime = 0.0f;
        startroomtime = 0.0f;
        endclasstime = 25.0f;
        endintro = 43.0f;

        fading = false;
        resuming = false;
        starting = false;
        ending = false;
        lookatsubject = false;
        stoplookingatsubject = false;
        welldone = false;
        teacher_response = false;
        notswapped = true;
    }

    // Update is called once per frame
    void Update()
    {

        if (fading)
        {
            fadetime += Time.deltaTime;
        }

        if (scenario_running)
        {
            scenetime += Time.deltaTime;
        }
        if (resuming)
        {
            resumetime += Time.deltaTime;
        }
        if (starting)
        {
            startroomtime += Time.deltaTime;
        }
        if (ending)
        {
            endtime += Time.deltaTime;
        }


        if (Input.GetKeyDown("m"))
        {
            starting = true;
            //play startroom instructions.
            changesound xbox = GameObject.Find("Xbox One").transform.GetComponent<changesound>();
            if (xbox.soundcount == 0)
            {
                xbox.playnext();
            }
        }

        if (Input.GetKeyDown("p"))
        {
            //start the scenario
            scenario_running = true;
            //fade = GameObject.Find("fadeobject").GetComponent<ScreenFader>();
            fade.fadeIn = true;

        }

        if (Input.GetKeyDown("k"))
        {
            //start the scenario
            resuming = true;
            stoplookingatsubject = true;

        }
        if (Input.GetKeyDown("j"))
        {
            //start the scenario
            welldone = true;
            resuming = true;
            stoplookingatsubject = true;

        }



        if (Input.GetKeyDown("f"))
        {
            //fading = true;
            //SteamVR_Fade.View(Color.black, 1);
            fade = GameObject.Find("fadeobject").GetComponent<ScreenFader>();
            fade.fadeIn = !fade.fadeIn;
        }

        if (startroomtime > endintro)
        {
            //fading = true;
            fade = GameObject.Find("fadeobject").GetComponent<ScreenFader>();
            fade.fadeIn = false;

        }

        if ((startroomtime > (endintro + 3.0f)) && notswapped)
        {

            notswapped = false;


            //swap room positions when dark
            Transform startplace = GameObject.Find("startroom").transform;
            startplace.transform.position = new Vector3(startplace.position.x, startplace.position.y + movefactor, startplace.position.z);
            transform.parent.position = new Vector3(transform.position.x, transform.position.y - movefactor, transform.position.z);
            movefactor = -1.0f * movefactor;



        }

        if (startroomtime > (endintro + 6.0f))
        {
            starting = false;
            startroomtime = 0.0f;
            fade.fadeIn = true;

            //start the scenario
            scenario_running = true;

        }



        if (lookatsubject)//Input.GetKeyDown("0"))
        {
            //get the attention of the classroom
            Transform p1 = GameObject.Find("people").transform.Find("heidi_flat");
            Transform p2 = GameObject.Find("people").transform.Find("man1");
            Transform p3 = GameObject.Find("people").transform.Find("man2");
            Transform p4 = GameObject.Find("people").transform.Find("woman3_classroom");
            Transform p5 = GameObject.Find("people").transform.Find("woman5_classroom");
            Transform p6 = GameObject.Find("people").transform.Find("man3_classroom");
            Transform p7 = GameObject.Find("people").transform.Find("teacher_flat");

            looker = p1.GetComponent<LookTargetController>();
            looker1 = p2.GetComponent<LookTargetController>();
            looker2 = p3.GetComponent<LookTargetController>();
            looker3 = p4.GetComponent<LookTargetController>();
            looker4 = p5.GetComponent<LookTargetController>();
            looker5 = p6.GetComponent<LookTargetController>();
            looker6 = p7.GetComponent<LookTargetController>();

            looker.lookAtPlayerRatio = 1.0f;
            looker1.lookAtPlayerRatio = 1.0f;
            looker2.lookAtPlayerRatio = 1.0f;
            looker3.lookAtPlayerRatio = 1.0f;
            looker4.lookAtPlayerRatio = 1.0f;
            looker5.lookAtPlayerRatio = 1.0f;
            looker6.lookAtPlayerRatio = 1.0f;

            lookatsubject = false;


        }

        if (stoplookingatsubject)
        {
            //get the attention of the classroom
            Transform p1 = GameObject.Find("people").transform.Find("heidi_flat").transform;
            Transform p2 = GameObject.Find("people").transform.Find("man1");
            Transform p3 = GameObject.Find("people").transform.Find("man2");
            Transform p4 = GameObject.Find("people").transform.Find("woman3_classroom");
            Transform p5 = GameObject.Find("people").transform.Find("woman5_classroom");
            Transform p6 = GameObject.Find("people").transform.Find("man3_classroom");
            Transform p7 = GameObject.Find("people").transform.Find("teacher_flat");


            looker = p1.GetComponent<LookTargetController>();
            looker1 = p2.GetComponent<LookTargetController>();
            looker2 = p3.GetComponent<LookTargetController>();
            looker3 = p4.GetComponent<LookTargetController>();
            looker4 = p5.GetComponent<LookTargetController>();
            looker5 = p6.GetComponent<LookTargetController>();
            looker6 = p7.GetComponent<LookTargetController>();

            looker.lookAtPlayerRatio = 0.1f;
            looker1.lookAtPlayerRatio = 0.1f;
            looker2.lookAtPlayerRatio = 0.1f;
            looker3.lookAtPlayerRatio = 0.1f;
            looker4.lookAtPlayerRatio = 0.1f;
            looker5.lookAtPlayerRatio = 0.1f;
            looker6.lookAtPlayerRatio = 0.1f;

            stoplookingatsubject = false;


        }



        if (scenetime > 5.0f)
        {
            //call the first line of dialog.
            if (GameObject.Find("teacher_flat").transform.GetComponent<changesound>().soundcount == 0)
            {
                GameObject.Find("teacher_flat").transform.GetComponent<changesound>().playnext();
            }
        }

        if (scenetime > 28.0f)
        {
            //call the first line of dialog.
            if (GameObject.Find("man1").transform.GetComponent<changesound>().soundcount == 0)
            {
                GameObject.Find("man1").transform.GetComponent<changesound>().playnext();
            }
        }

        if (scenetime > 41.0f)
        {
            //call the first line of dialog.
            if (GameObject.Find("teacher_flat").transform.GetComponent<changesound>().soundcount == 1)
            {
                GameObject.Find("teacher_flat").transform.GetComponent<changesound>().playnext();
            }
        }

        if (scenetime > 48.0f)
        {
            //call the first line of dialog.
            if (GameObject.Find("heidi_flat").transform.GetComponent<changesound>().soundcount == 0)
            {
                GameObject.Find("heidi_flat").transform.GetComponent<changesound>().playnext();
            }
        }

        if (scenetime > 61.0f)
        {
            //call the first line of dialog.
            if (GameObject.Find("teacher_flat").transform.GetComponent<changesound>().soundcount == 2)
            {
                GameObject.Find("teacher_flat").transform.GetComponent<changesound>().playnext();
                //turn on the look at for the classmates to look at the patient.
                lookatsubject = true;
            }
        }

        if (resumetime > 0.1f)
        {
            if (!teacher_response)
            {
                //resume dialog.
                if (welldone)
                {
                    //well done you spoke.
                    GameObject.Find("teacher_flat").transform.GetComponent<changesound>().soundcount = 6;
                    GameObject.Find("teacher_flat").transform.GetComponent<changesound>().playnext();
                }
                else
                {
                    //see me after class!
                    GameObject.Find("teacher_flat").transform.GetComponent<changesound>().soundcount = 5;
                    GameObject.Find("teacher_flat").transform.GetComponent<changesound>().playnext();
                }
                teacher_response = true;
            }

        }
        if (resumetime > 4.0f)
        {
            if (welldone)
            {
                endclasstime = 5.0f;
                /*
                //call the first line of dialog.
                if (GameObject.Find("man2").transform.GetComponent<changesound>().soundcount == 0)
                {
                    GameObject.Find("man2").transform.GetComponent<changesound>().playnext();
                }
                */
            }
            else
            {
                endclasstime = 8.0f;
            }
        }
        if (resumetime > endclasstime)
        {
            //fade = GameObject.Find("fadeobject").GetComponent<ScreenFader>();
            fade.fadeIn = false;
        }
        if ((resumetime > (endclasstime + 4.0f)) && (fade.fadeIn == false))
        {
            //fade = GameObject.Find("fadeobject").GetComponent<ScreenFader>();
            fade.fadeIn = true;
            resumetime = 0.0f;
            resuming = false;
            ending = true;
            endtime = 0.0f;

            //swap room positions when dark
            Transform startplace = GameObject.Find("startroom").transform;
            startplace.transform.position = new Vector3(startplace.position.x, startplace.position.y + movefactor, startplace.position.z);
            transform.parent.position = new Vector3(transform.position.x, transform.position.y - movefactor, transform.position.z);
            //movefactor = -1.0f * movefactor;

        }
        if ((endtime > 3.0f))
        {

            changesound xbox = GameObject.Find("Xbox One").transform.GetComponent<changesound>();
            if (xbox.soundcount == 1)
            {
                xbox.playnext();
            }

        }

    }
}