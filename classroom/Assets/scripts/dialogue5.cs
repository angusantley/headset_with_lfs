﻿using UnityEngine;
using System.Collections;

public class dialogue5 : MonoBehaviour {

    changesound teacher;
    float endtiming;
    bool endnow;
    

	// Use this for initialization
	void Start () {

        teacher = GameObject.Find("teacher_flat").transform.GetComponent<changesound>();
        endtiming = 0.0f;
        endnow = false;
	
	}
	
	// Update is called once per frame
	void Update () {

        if (fades.scenetime > 5.0f)
        {
            //call the first line of dialog.
            if (teacher.soundcount == 0)
            {
                teacher.playnext();

            }
        }

        //24.4 length of previous
        if (fades.scenetime > 30.0f)
        {
            //call the first line of dialog.
            if (teacher.soundcount == 1)
            {
                teacher.playnext();

            }
        }

        //9 seconds length

        if ( Input.GetKeyDown("j") )
        {
            fades.scene_end = true;
        }


        if (Input.GetKeyDown("k"))
        {

            if (teacher.soundcount == 2)
            {
                teacher.playnext();

                endtiming = fades.scenetime;
                endnow = true;

            }
            
        }

        if ( ( fades.scenetime > endtiming + 7.0f ) && endnow )
        {
            endnow = false;
            fades.scene_end = true;
        }
	
	}
}
