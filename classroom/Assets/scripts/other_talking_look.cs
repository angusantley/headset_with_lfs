﻿using UnityEngine;
using System.Collections;
using RealisticEyeMovements;

public class other_talking_look : MonoBehaviour {

    [System.Serializable]
    public class others_talk
    {
        public string Name;
        public AudioSource other_person_audioplayer;
        public AudioClip other_person_line;
        public Transform[] lookats;
        public float[] triggerlooks;
        public float[] lookdurations;
        [HideInInspector]
        public bool[] notlooked;
        
    }

    private LookTargetController looker;

    public others_talk[] looking_while_others_talk;
    

   

	// Use this for initialization
	void Start () {

        looker = transform.GetComponent<LookTargetController>();

        for (int j = 0; j < looking_while_others_talk.Length; j++)
        {

            looking_while_others_talk[j].notlooked = new bool[looking_while_others_talk[j].triggerlooks.Length];

            for (int l = 0; l < looking_while_others_talk[j].triggerlooks.Length; l++)
            {
                looking_while_others_talk[j].notlooked[l] = true;

            }
        }
	
	}
	
	// Update is called once per frame
	void Update () {

        for (int i = 0; i < looking_while_others_talk.Length; i++)
        {


            if (looking_while_others_talk[i].other_person_audioplayer.isPlaying)
            {

                for (int j = 0; j < looking_while_others_talk[i].triggerlooks.Length; j++)
                {
                    //if (conversation_responses[i].character_line.name
                    if ((looking_while_others_talk[i].other_person_audioplayer.clip.name == looking_while_others_talk[i].other_person_line.name) && (looking_while_others_talk[i].other_person_audioplayer.time > looking_while_others_talk[i].triggerlooks[j]) && looking_while_others_talk[i].notlooked[j])
                    {
                        looking_while_others_talk[i].notlooked[j] = false;
                        Debug.Log("other talking response i " + i + " look at j " + j + transform.name.ToString());
                        Debug.Log("other talking look duration for " + transform.name.ToString() + " is "  + looking_while_others_talk[i].lookdurations[j]);
                        looker.LookAtPoiDirectly(looking_while_others_talk[i].lookats[j], looking_while_others_talk[i].lookdurations[j], 0.01f);

                    }
                }
            }
            else
            {
                for (int l = 0; l < looking_while_others_talk[i].triggerlooks.Length; l++)
                {
                    looking_while_others_talk[i].notlooked[l] = true;

                }
            }
        }
	
	}


}
