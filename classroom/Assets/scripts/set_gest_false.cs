﻿using UnityEngine;
using System.Collections;

public class set_gest_false : StateMachineBehaviour
{

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("L1", false);
        animator.SetBool("L2", false);
        animator.SetBool("L3", false);
        animator.SetBool("L4", false);
        animator.SetBool("L5", false);
        animator.SetBool("R1", false);
        animator.SetBool("R2", false);
        animator.SetBool("R3", false);
        animator.SetBool("R4", false);
    }
	
}
