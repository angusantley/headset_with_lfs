﻿using UnityEngine;
using System.Collections;

public class dialogue_updated : MonoBehaviour {

    [System.Serializable]
    public class actor_line_time
    {
        public string Name;
        public Transform actor;
        public AudioClip actor_line;
        public float line_trigger_time;
        public bool decisionpoint;

        [HideInInspector]
        public bool notspoken;
    }
    public actor_line_time[] screenplay;
    public Transform jperson;
    public Transform kperson;
    public AudioClip jtalk;
    public AudioClip ktalk;
    bool welldone;

    bool dodormdemo;
    bool doclassdemo;

    public static float scenetime;
    public static bool scenario_running;
    public static float scene_end_time;
    public float end_time;


    // Use this for initialization
    void Start()
    {

        for (int k = 0; k < screenplay.Length; k++)
        {
            screenplay[k].notspoken = true;
        }

        welldone = false;
        scene_end_time = end_time;


        scenetime = 0.0f;


    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("m"))
        {
            scenario_running = true;
        }

        if (scenario_running)
        {
            scenetime += Time.deltaTime;
        }

        if (Input.GetKeyDown("j"))
        {
            //positive ending to scenario
            //onboard_fades.scene_end = true;
            welldone = true;
            //restart scenario running
            scenario_running = true;

            // AudioSource actorvoice = screenplay[j].actor.GetComponent<AudioSource>();
            // changesound currentactorlines = screenplay[j].actor.GetComponent<changesound>();
            //Animator actor_animator = screenplay[j].actor.GetComponent<Animator>();
            jperson.GetComponent<Animator>().SetBool("stand", true);
            jperson.GetComponent<Animator>().SetBool("sit", false);
            jperson.GetComponent<AudioSource>().clip = jtalk;
            jperson.GetComponent<AudioSource>().Play();
            transform.root.GetComponent<grouplookingbehaviour>().allawayfromparticipant();
        }


        if (Input.GetKeyDown("k"))
        {

            //outcome: participant did not act
            welldone = false;
            //restart scenario running
            scenario_running = true;
            jperson.GetComponent<Animator>().SetBool("stand", true);
            jperson.GetComponent<Animator>().SetBool("sit", false);
            jperson.GetComponent<AudioSource>().clip = ktalk;
            jperson.GetComponent<AudioSource>().Play();
            transform.root.GetComponent<grouplookingbehaviour>().allawayfromparticipant();

        }
        //code just for scenario 19
        if (Input.GetKeyDown("3"))
        {
            //scenario_running = true;
        }




        for (int j = 0; j < screenplay.Length; j++)
        {
            //is it past time for this line? and has it yet to be spoken?
            if ((scenetime > screenplay[j].line_trigger_time) && (screenplay[j].notspoken))
            {

                screenplay[j].notspoken = false;
                //trigger the line.
                AudioSource actorvoice = screenplay[j].actor.GetComponent<AudioSource>();
                changesound currentactorlines = screenplay[j].actor.GetComponent<changesound>();
                Animator actor_animator = screenplay[j].actor.GetComponent<Animator>();
                bool soundfound = false;

                for (int i = 0; i < currentactorlines.clips.Length; i++)
                {
                    if (currentactorlines.clips[i].name == screenplay[j].actor_line.name)
                    {
                        soundfound = true;
                        if (screenplay[j].decisionpoint)
                        {
                            scenario_running = false;
                            //this line only good for scenerio 6 and 7
                            //need to change this.
                            transform.root.GetComponent<grouplookingbehaviour>().allatparticipant();
                        }
                        actorvoice.clip = currentactorlines.clips[i];
                        actorvoice.Play();
                        if (actor_animator)
                        {
                            //this would be better to happen with the actor.
                            //in the future, they will know if they need to move when they speak.
                            //we are doing this here because we start the sound here
                            //which happens only once.

                            if (currentactorlines.triggermotion[i])
                            {
                                Debug.Log("set stand true " + currentactorlines.clips[i].name);
                                actor_animator.SetBool("stand", true);
                                actor_animator.SetBool("sit", false);
                            }
                        }
                    }

                }
                if (!soundfound)
                {
                    //error this actor doesn't know this line
                    Debug.Log("line name not found in actor's line list!!");
                }


            }


        }

    }
}