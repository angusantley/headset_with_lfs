﻿using UnityEngine;
using System.Collections;

public class adjustrootself : MonoBehaviour {

    public Transform userBody;
    public Transform userHead;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void LateUpdate () {

        Transform selfavatar = userBody;    //GameObject.Find("mason_tpose").transform;
        Transform dk2headposition = GameObject.Find("Main Camera").transform.FindChild("atlanto_occipital").transform;
        Vector3 length_of_self = userHead.position - selfavatar.position;
        Vector3 newselfposition = dk2headposition.position - length_of_self;
        selfavatar.position = newselfposition;


    }
}
