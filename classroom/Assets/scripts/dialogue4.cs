﻿using UnityEngine;
using System.Collections;

public class dialogue4 : MonoBehaviour {

    changesound teacher;
    changesound penman;
    changesound heidi;
    float endtiming;
    float voicetime;
    bool endnow;

	// Use this for initialization
	void Start () {
	        teacher = GameObject.Find("teacher_flat").transform.GetComponent<changesound>();
            penman = GameObject.Find("man2").transform.GetComponent<changesound>();
            heidi = GameObject.Find("heidi_flat").transform.GetComponent<changesound>();
            endtiming = 0.0f;
            endnow = false;
            voicetime = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {

        if (fades.scenetime > 5.0f)
        {
            //call the first line of dialog.
            if (heidi.soundcount == 0)
            {
                heidi.playnext();

            }
        }

        if (fades.scenetime > 11.0f)
        {
            //call the first line of dialog.
            if (teacher.soundcount == 0)
            {
                teacher.playnext();

            }
        }

        //a,s,d for andy response
        //z,x,c for heidi response

        //right now we decide when to end it. or we could hard code it after the last person 
        //agrees to give over the pen.

        if (Input.GetKeyDown("j"))
        {
            if (penman.soundcount == 0)
            {
                penman.playnext();

            }
            //OK I'll give you some extra time since some of you didn't have a pen
            //fades.scene_end = true;
            endtiming = fades.scenetime;
            endnow = true;
            voicetime = 3.0f;

        }

        if (Input.GetKeyDown("k"))
        {

            if (teacher.soundcount == 1)
            {
                //teacher.soundcount = 2;
                teacher.playnext();

            }
            //I will collect your papers now.
            //fades.scene_end = true;
            endtiming = fades.scenetime;
            endnow = true;
            voicetime = 11.0f;
        }


        //change length from 7.0 here
        if ((fades.scenetime > endtiming + voicetime) && endnow)
        {
            endnow = false;
            fades.scene_end = true;
        }
	
	}
}
