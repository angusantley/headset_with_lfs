﻿using UnityEngine;
using System.Collections;
using RealisticEyeMovements;

public class look_talk_hand : MonoBehaviour
{

    LookTargetController looker;
    AudioSource audioplayer;
    Animator anim;


    [System.Serializable]
    public class bag
    {
        public string Name;
        public AudioClip character_line;
        public Transform[] lookats;
        public float[] triggerlooks;
        public float[] lookdurations;
        public float[] triggerhand;
        public string[] gesturechoice;
        [HideInInspector]
        public bool[] notlooked;
        [HideInInspector]
        public bool[] notgestured;
        //public bool response;
    }

    public bag[] conversation_responses;
    bool soundwasjustplaying;

    // Use this for initialization
    void Start()
    {

        soundwasjustplaying = false;
        looker = transform.GetComponent<LookTargetController>();
        audioplayer = transform.GetComponent<AudioSource>();
        anim = transform.GetComponent<Animator>();

        //conversation_responses[0].notgestured = new bool[conversation_responses[0].triggerhand.Length];

        for (int j = 0; j < conversation_responses.Length; j++)
        {
            

            conversation_responses[j].notgestured = new bool[conversation_responses[j].triggerhand.Length];
            conversation_responses[j].notlooked = new bool[conversation_responses[j].triggerlooks.Length];
            for (int k = 0; k < conversation_responses[j].triggerhand.Length; k++)
            {
                conversation_responses[j].notgestured[k] = true;
                
                Debug.Log("gesture choice " + conversation_responses[j].gesturechoice[k]);

            }
            for (int l = 0; l < conversation_responses[j].triggerlooks.Length; l++)
            {
                conversation_responses[j].notlooked[l] = true;

            }
        }


        if (anim)
        {
            //anim.SetBool("sit", false);
            //anim.SetBool("stand", true);
            anim.SetBool("hand", false);
            anim.SetBool("L1", false);
            anim.SetBool("L2", false);
            anim.SetBool("L3", false);
            anim.SetBool("L4", false);
            anim.SetBool("L5", false);
            anim.SetBool("R1", false);
            anim.SetBool("R2", false);
            anim.SetBool("R3", false);
            anim.SetBool("R4", false);
        }



    }




    // Update is called once per frame
    void Update()
    {
        if (anim)
        {
            if (anim.GetBool("hand"))
            {
                anim.SetBool("hand", false);
            }
            if (anim.GetBool("L1"))
            {
                anim.SetBool("L1", false);
            }
            if (anim.GetBool("L2"))
            {
                anim.SetBool("L2", false);
            }
            if (anim.GetBool("L3"))
            {
                anim.SetBool("L3", false);
            }
            if (anim.GetBool("L4"))
            {
                anim.SetBool("L4", false);
            }
            if (anim.GetBool("L5"))
            {
                anim.SetBool("L5", false);
            }
            if (anim.GetBool("R1"))
            {
                anim.SetBool("R1", false);
            }
            if (anim.GetBool("R2"))
            {
                anim.SetBool("R2", false);
            }
            if (anim.GetBool("R3"))
            {
                anim.SetBool("R3", false);
            }
            if (anim.GetBool("R4"))
            {
                anim.SetBool("R4", false);
            }

        }

        if (audioplayer.isPlaying)
        {
            
            Debug.Log("audio is playing");
            soundwasjustplaying = true;
            //anim.SetBool("stand", true);
            
            for (int i = 0; i < conversation_responses.Length; i++)
            {
                

                for (int j = 0; j < conversation_responses[i].triggerlooks.Length; j++)
                {
                    //if (conversation_responses[i].character_line.name
                    if ((audioplayer.clip.name == conversation_responses[i].character_line.name) && (audioplayer.time > conversation_responses[i].triggerlooks[j]) && conversation_responses[i].notlooked[j])
                    {
                        conversation_responses[i].notlooked[j] = false;
                        Debug.Log("look hand response i " + i + " look at j " + j + transform.name.ToString());
                        looker.LookAtPoiDirectly(conversation_responses[i].lookats[j], conversation_responses[i].lookdurations[j], 0.01f);

                    }
                }
                for (int k = 0; k < conversation_responses[i].triggerhand.Length; k++)
                {
                    if ((audioplayer.clip.name == conversation_responses[i].character_line.name) && (audioplayer.time > conversation_responses[i].triggerhand[k]) && conversation_responses[i].notgestured[k])
                    {
                        conversation_responses[i].notgestured[k] = false;
                        Debug.Log("line " + i + " hand gesture for k " + k + transform.name.ToString());
                        if (anim)
                        {
                            Debug.Log("set gesture true" + conversation_responses[i].gesturechoice[k]);
                            //anim.SetBool("hand", true);
                            //this should be L1 or R1 etc.
                            anim.SetBool( conversation_responses[i].gesturechoice[k], true );
                            //anim.SetBool("sit", false);
                        }
                        //set stand true;


                    }
                }
            }

        }
        else  //sound has stopped playing
        {
            if (anim && soundwasjustplaying)// && !(conversation_responses[i].notgestured[(conversation_responses[i].triggerhand.Length) - 1]) && !(conversation_responses[i].notlooked[(conversation_responses[i].triggerlooks.Length) - 1]))
            {
                soundwasjustplaying = false;
                //exit base talk
                //this is ok to set sit true when the audio is done.
                Debug.Log("set sit true");
                anim.SetBool("sit", true);
                //anim.SetBool("stand", false);
                

                //reset the triggers so that we could call this line again if we would like to
                for (int i = 0; i < conversation_responses.Length; i++)
                {
                    for (int l = 0; l < conversation_responses[i].triggerhand.Length; l++)
                    {
                        conversation_responses[i].notgestured[l] = true;

                    }
                    for (int j = 0; j < conversation_responses[i].triggerlooks.Length; j++)
                    {
                        conversation_responses[i].notlooked[j] = true;

                    }
                }
            }
                      
        }


    }
}
