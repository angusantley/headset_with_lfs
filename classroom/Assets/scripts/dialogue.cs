﻿using UnityEngine;
using System.Collections;

public class dialogue : MonoBehaviour {

    changesound teacher;
    changesound woman5;
    changesound heidi_flat;
    changesound man1;
    changesound man2;

    bool endscript;

	// Use this for initialization
	void Start () {

        teacher = GameObject.Find("teacher_flat").transform.GetComponent<changesound>();
        woman5 = GameObject.Find("woman5_classroom").transform.GetComponent<changesound>();
        man1 = GameObject.Find("man1").transform.GetComponent<changesound>();
        man2 = GameObject.Find("man2").transform.GetComponent<changesound>();
        heidi_flat = GameObject.Find("heidi_flat").transform.GetComponent<changesound>();

        endscript = false;
	}
	
	// Update is called once per frame
	void Update () {

        //Debug.Log("scenetime is " + fades.scenetime);

        if ( fades.scenetime > 5.0f)
        {
            //call the first line of dialog.
            if (teacher.soundcount == 0)
            {
                teacher.playnext();

            }
        }

        //11.4 length of previous
        if (fades.scenetime > 17.0f)
        {
            //call the first line of dialog.
            if (teacher.soundcount == 1)
            {
                teacher.playnext();
                fades.scenario_running = false;

            }
            //pause the scene clock while the user answers.
            
        }

        //pause here for user answer
        //remember that 6,7,8 are the teacher prompts.
        //resume with j for no answer, k for answer

        //5.3 length of previous
        if ( Input.GetKeyDown("k") )              //fades.scenetime > 24.0f)
        {
            //restart the scene clock 
            //I guess  you want space travel
            fades.scenario_running = true;
            //call the first line of dialog.
            if (teacher.soundcount == 2)
            {
                teacher.playnext();
                teacher.soundcount = 4;

            }
        }

        //5.3 again length of previous
        if ( Input.GetKeyDown("j") )
        {
            //restart the scene clock 
            //I guess you want the same
            fades.scenario_running = true;
            //call the first line of dialog.
            if (teacher.soundcount == 3)
            {
                teacher.playnext();

            }
            else 
            {
                teacher.soundcount = 3;
                teacher.playnext();
            }
        }

        //5.6 length of previous
        if (fades.scenetime > 20.0f)
        {

            
            //abi saying yes
            if (woman5.soundcount == 0)
            {
                Debug.Log("in woman 5 sound");
                woman5.playnext();

            }
        }

        //1.5 length of previous
        if (fades.scenetime > 25.0f)
        {
           
            //whiner 1
            if (man1.soundcount == 0)
            {
                Debug.Log("in man 1 sound");
                man1.playnext();

            }
        }

        //6.7 length of previous
        if (fades.scenetime > 32.0f)
        {
            //whiner 2
            if (man2.soundcount == 0)
            {
                man2.playnext();

            }
        }


        //7.3 length of previous
        if (fades.scenetime > 42.0f)
        {
            //whiner 3
            if (heidi_flat.soundcount == 0)
            {
                heidi_flat.playnext();

            }
        }

        //4.6 again length of previous
        if (fades.scenetime > 48.0f)
        {
            //call the first line of dialog.
            if (teacher.soundcount == 4)
            {
                teacher.playnext();

            }
        }



        //last speech is 11.4 sec.
        if (fades.scenetime > 65.0f && !endscript)
        {
            endscript = true;
            fades.scene_end = true;
        }






	
	}
}
