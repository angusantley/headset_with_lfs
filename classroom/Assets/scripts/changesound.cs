﻿using UnityEngine;
using System.Collections;

public class changesound : MonoBehaviour {

    public AudioClip mlclip;
    public AudioClip[] clips;

    [HideInInspector]
    public int soundcount;
    Animator anim;
    public char[] speakKey;
    public bool[] triggermotion;
    bool startedsound;


    public void playnext()
    {
        //Debug.Log("print of anim value " + anim.ToString());
        if (anim)
        {
            Debug.Log("made it to set stand true");
            anim.SetBool("stand", true);
            anim.SetBool("sit", false);
            startedsound = true;
        }
        AudioSource voice = transform.GetComponent<AudioSource>();
        voice.clip = clips[soundcount];
        voice.Play();
        soundcount++;
    }

	// Use this for initialization
	void Start () {
        startedsound = false;
        anim = null;
        soundcount = 0;
        //clips[0] = new AudioClip();
        //clips[1] = new AudioClip();
        // mlclip.LoadAudioData();

        //speakKey = new char[clip]

        anim = transform.GetComponent<Animator>();
        if (anim)
        {
            anim.SetBool("stand", false);
            anim.SetBool("sit", false);
        }

        //transform.GetComponent<AudioClip>().LoadAudioData(mlclip);
        //transform.GetComponent<AudioSource>().Play();//
        
	
	}
	
	// Update is called once per frame
	void Update () {

        AudioSource temp = transform.GetComponent<AudioSource>();
        if (anim)
        {
            
            if ((temp.isPlaying) )
            {
                /*
                if (anim.GetBool("stand"))
                {
                    anim.SetBool("stand", false);
                }
                if (anim.GetBool("sit"))
                {
                    anim.SetBool("sit", false);
                }
                */
            }
            else //!temp.isPlaying
            { 
                anim.SetBool("sit", true); 
            }

            if (startedsound)
            {
                startedsound = false;
              //  anim.SetBool("stand", true);
              //  anim.SetBool("sit", false);

            }
        }

        for (int i = 0; i < speakKey.Length; i++) {

            if (Input.GetKeyDown(speakKey[i].ToString()))
            {
                // transform.GetComponent<AudioSource>().Play();
                //transform.GetComponent<AudioClip>().LoadAudioData(mlclip);
                if (triggermotion[i])
                {
                    if (anim)
                    {
                        anim.SetBool("stand", true);
                        anim.SetBool("sit", false);
                    }
                }
                //temp.clip = mlclip;
                //temp.PlayOneShot(clips[i]);
                temp.clip = clips[i];
                temp.Play();
                //soundcount++;
                if (soundcount == clips.Length)
                {
                    //soundcount = 0;
                }
                
            }

        }
        
	
	}
}
