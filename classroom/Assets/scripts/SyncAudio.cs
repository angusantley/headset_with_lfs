﻿using UnityEngine;
using System.Collections;

public class SyncAudio : StateMachineBehaviour {

	public AudioClip clip; 

	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		if(clip){
			AudioSource audio = animator.gameObject.GetComponent<AudioSource> ();
			if(audio){
				audio.clip = clip;
				audio.Play ();
			}
		}
	}
}
