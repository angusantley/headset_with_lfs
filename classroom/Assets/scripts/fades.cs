﻿using UnityEngine;
using System.Collections;

public class fades : MonoBehaviour
{


    public float end_intro;
    public float end_outro;
    

    ScreenFader fade;
    changesound xbox;

    Transform startplace;
    Transform classplace;

    float fadetime;
    float startroomtime;
    public static float scenetime;
    float endtime;
    float scene_length;

    float movefactor;

    bool fading;
    bool starting;
    public static bool scenario_running;
    bool ending;
    bool notswapped;
    public static bool scene_end;

    // Use this for initialization
    void Start()
    {

        fading = false;
        starting = false;
        scenario_running = false;
        ending = false;
        scene_end = false;
        notswapped = true;


        fadetime = 0.0f;
        startroomtime = 0.0f;
        scenetime = 0.0f;
        endtime = 0.0f;

        scene_length = 600.0f;

        movefactor = 1000.0f;

        fade = GameObject.Find("rootobject").GetComponent<ScreenFader>();
        xbox = GameObject.Find("Xbox One").transform.GetComponent<changesound>();
        startplace = GameObject.Find("startroom").transform;
        classplace = GameObject.Find("classroom").transform;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown("m"))
        {
            starting = true;
            if (xbox.soundcount == 0)
            {
                xbox.playnext();
            }
        }

        if (Input.GetKeyDown("p"))
        {
            ending = true;
            xbox.playnext();

        }


        if (fading)
        {
            fadetime += Time.deltaTime;
        }

        if (starting)
        {
            startroomtime += Time.deltaTime;
        }

        if (scenario_running)
        {
            scenetime += Time.deltaTime;
        }

        if (ending)
        {
            endtime += Time.deltaTime;
        }

        if (startroomtime > end_intro)
        {
            fade.fadeIn = false;

        }

        if ((startroomtime > (end_intro + 3.0f)) && notswapped)
        {
            //swap room positions when dark
            startplace.transform.position = new Vector3(startplace.position.x, startplace.position.y + movefactor, startplace.position.z);
            classplace.transform.position = new Vector3(classplace.position.x, classplace.position.y - movefactor, classplace.position.z);
            movefactor = -1.0f * movefactor;
            notswapped = false;
        }




        if ( startroomtime > (end_intro + 6.0f) )
        {
            //end startroom
            starting = false;
            startroomtime = 0.0f;
            
            //begin classroom
            scenario_running = true;
            fade.fadeIn = true;

        }


        //this is all the dialogue added together with pauses
        if ( scene_end )
        {
            scene_end = false;
            //end scenario here.
            fade.fadeIn = false;
            scene_length = scenetime;
            Debug.Log("scene length " + scene_length);
        }

        if ( scenetime > ( scene_length + 4.0f ) )    //previous time plus six secs.
        {
            scenario_running = false;
            scenetime = 0.0f;

            //swap rooms while dark
            startplace.transform.position = new Vector3(startplace.position.x, startplace.position.y + movefactor, startplace.position.z);
            classplace.transform.position = new Vector3(classplace.position.x, classplace.position.y - movefactor, classplace.position.z);

            ending = true;

            fade.fadeIn = true;
        }

        if ( endtime > 2.0f )
        {
            if (xbox.soundcount == 1)
            {
                xbox.playnext();
            }

        }

        //exit talk is 35 sec
        if ( endtime > ( end_outro + 2.0f ) )  
        {

            fade.fadeIn = false;
            
        }

        




    }
}
