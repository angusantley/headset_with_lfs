﻿using UnityEngine;
using System.Collections;
using RealisticEyeMovements;


public class dialogue3 : MonoBehaviour {

    changesound teacher;
    changesound man1;
    changesound man2;

    LookTargetController looker1;
    LookTargetController looker2;
    LookTargetController teacherlooker;

    bool angrylook1;
    bool angrylook2;

    Transform user;
    Transform outsidedoor;

    //for the teacher walkout to be triggered
    Animator anim;

    bool endscript;
    bool walkout;


	// Use this for initialization
	void Start () {

        teacher = GameObject.Find("teacher_flat").transform.GetComponent<changesound>();
        man1 = GameObject.Find("man1").transform.GetComponent<changesound>();
        man2 = GameObject.Find("man2").transform.GetComponent<changesound>();

        user = GameObject.Find("Main Camera").transform;

        outsidedoor = GameObject.Find("outsidedoor").transform;

        Transform man4 = GameObject.Find("people").transform.Find("man4_classroom");
        Transform woman5 = GameObject.Find("people").transform.Find("woman5_classroom");
        looker1 = man4.GetComponent<LookTargetController>();
        looker2 = woman5.GetComponent<LookTargetController>();
        teacherlooker = GameObject.Find("teacher_flat").transform.GetComponent<LookTargetController>();

        angrylook1 = false;
        angrylook2 = false;


        endscript = false;
        walkout = false;

        //get the teacher animator to trigger the walkout
        anim = teacher.transform.GetComponent<Animator>();
        if (anim)
        {
            anim.SetBool("leave", false);
            anim.SetBool("return", false);
        }
        
	
	}
	
	// Update is called once per frame
	void Update () {

        //for the teacher walkout trigger
        if (anim)
        {
            if (anim.GetBool("leave"))
            {
                anim.SetBool("leave", false);
            }
            if (anim.GetBool("return"))
            {
                anim.SetBool("return", false);
            }
        }

        if (Input.GetKeyDown("y"))
        {
            //anim.SetBool("leave", true);
            looker2.LookAtPoiDirectly(user, 2.0f, 0.01f);
        }

        if (Input.GetKeyDown("u"))
        {
            //anim.SetBool("return", true);
            looker1.LookAtPoiDirectly(user, 2.0f, 0.01f);
        }


        if (fades.scenetime > 5.0f)
        {
            //call the first line of dialog.
            if (teacher.soundcount == 0)
            {
                teacher.playnext();

            }
        }

        //6.2 length of previous
        if (fades.scenetime > 23.0f)
        {
            //call the first line of dialog.
            if (teacher.soundcount == 1)
            {
                teacher.playnext();

            }
        }

        //12.8 length of previous
        if (fades.scenetime > 34.3f)
        {
            //call the first line of dialog.
            if (man1.soundcount == 0)
            {
                man1.playnext();

            }
        }

        //teacher walks out
        if ( ( fades.scenetime > 39.0f ) && ( !walkout ) )
        {

            walkout = true;

            //disable teacher lookat.
            teacherlooker.LookAtPoiDirectly( outsidedoor, 10.0f, 0.01f );
            anim.SetBool("leave", true);
        }

        //2.8 again length of previous
        if (fades.scenetime > 52.0f)
        {
            //call the first line of dialog.
            if (man2.soundcount == 0)
            {
                man2.playnext();
                

            }
        }

        if ( (fades.scenetime > 55.0) && !angrylook1)
        {
            looker1.LookAtPoiDirectly(user, 4.0f, 0.01f);
            angrylook1 = true;
        }


        //9.0 length of previous
        if (fades.scenetime > 73.0f)
        {
            //call the first line of dialog.
            if (man2.soundcount == 1)
            {
                man2.playnext();
                

            }
        }

        if ((fades.scenetime > 76.0) && !angrylook2)
        {
            looker2.LookAtPoiDirectly(user, 4.0f, 0.01f);
            angrylook2 = true;
        }


        //teacher walks back in
        if (fades.scenetime > 76.0f)
        {
            anim.SetBool("return", true); 
        }

        //6.4 length of previous
        if (fades.scenetime > 83.0f)
        {
            //call the first line of dialog.
            if (teacher.soundcount == 2)
            {
                teacher.playnext();

            }
        }

        //4.1 length of previous
        if (fades.scenetime > 85.0f)
        {
            //call the first line of dialog.
            if (man1.soundcount == 1)
            {
                man1.playnext();

            }
        }


        //4.8 length of previous
        if (fades.scenetime > 92.0f)
        {
            //call the first line of dialog.
            if (teacher.soundcount == 3)
            {
                teacher.playnext();

            }
        }

        //length 7.2 secs : 68 total
        
        if (fades.scenetime > 100.0f && !endscript)
        {
            endscript = true;
            fades.scene_end = true;
        }
	
	}
}
